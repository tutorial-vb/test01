﻿Imports System.Windows.Forms.DataVisualization.Charting

Public Class Form1
    Public Amp As Single
    Public t As Single
    Public TT As Single
    Public x As Single
    Public y As Single
    Public damp As Single
    Public Text_out As String = "X Values;Y Values" + vbLf
    Public mydate As String
    Public filepfad As String

    Private Sub _Start()

        mydate = Date.Now.ToString
        mydate = mydate.Replace(".", "-")
        mydate = mydate.Replace(":", "-")
        mydate = mydate.Replace(" ", "_")
        filepfad = My.Computer.FileSystem.CurrentDirectory + "\" + mydate + ".csv"
        My.Computer.FileSystem.WriteAllText(filepfad, "t;X;Y" + vbLf, False)
        Timer1.Start()
        Button1.Text = "STOP"
    End Sub

    Private Sub _Stop()
        Timer1.Stop()
        Button1.Text = "START"
    End Sub

    Private Sub Graph_X_Draw()
        Dim MyGraphic As Graphics = CreateGraphics()
        Dim redPen As New Drawing.Pen(Color.Red, 2)
        Dim greenPen As New Drawing.Pen(Color.Green, 2)
        Dim blackPen As New Drawing.Pen(Color.Black, 1)

        If (((t * 100) Mod (Me.Size.Width - 22)) < 1) Then
            MyGraphic.Clear(Me.BackColor)
        End If

        MyGraphic.DrawLine(blackPen, 20, 200, 800, 200)
        MyGraphic.DrawLine(greenPen, (20 + t * 100) Mod (Me.Size.Width - 22), 200 - x * 100 / Amp, (22 + t * 100) Mod (Me.Size.Width - 22), 202 - x * 100 / Amp)

        MyGraphic.DrawLine(blackPen, 20, 350, 800, 350)
        MyGraphic.DrawLine(redPen, (20 + t * 100) Mod (Me.Size.Width - 22), 350 + y * 100 / Amp, (22 + t * 100) Mod (Me.Size.Width - 22), 352 + y * 100 / Amp)


    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If (Timer1.Enabled = True) Then
            _Stop()
        Else
            _Start()
        End If
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick

        If (IsNumeric(TextAmp.Text) And IsNumeric(TextPer.Text) And IsNumeric(TextTime.Text) And IsNumeric(TextDump.Text)) Then

            Amp = Convert.ToSingle(TextAmp.Text)
            TT = Convert.ToSingle(TextPer.Text)
            t = Convert.ToSingle(TextTime.Text)
            damp = Convert.ToSingle(TextDump.Text)

            If (TT <= 0) Then
                _Stop()
                MsgBox("T <= 0 is not allowed", vbOKOnly, "ERROR")
            ElseIf (t < 0) Then
                _Stop()
                MsgBox("t < 0 is not allowed", vbOKOnly, "ERROR")
            Else
                Form2.Show()
                Form2.Pendel_Draw()
                Graph_X_Draw()
                Label_X.Text = "X = " + x.ToString
                Label_Y.Text = "Y = " + y.ToString
                Text_out = t.ToString + ";" + x.ToString + ";" + y.ToString + vbLf
                My.Computer.FileSystem.WriteAllText(filepfad, Text_out, True)
                LabelInfo.Text = "INFO : " + filepfad + " is saved."
            End If

            TextTime.Text = (Convert.ToSingle(TextTime.Text) + 0.01).ToString
        Else
            _Stop()
            MsgBox("Fields must be numeric", vbOKOnly, "ERROR")
        End If

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim MyGraphic As Graphics = CreateGraphics()
        _Stop()
        Form2.Close()
        TextAmp.Text = "250"
        TextDump.Text = "0"
        TextPer.Text = "1"
        TextTime.Text = "0"
        Label_X.Text = "X = "
        Label_Y.Text = "Y = "
        MyGraphic.Clear(Me.BackColor)
    End Sub

    Private Sub TextAmp_MouseHover(sender As Object, e As EventArgs) Handles TextAmp.MouseHover
        LabelInfo.Text = "INFO : Please enter amplitude. Use comma as decimal point."
    End Sub

    Private Sub TextAmp_MouseLeave(sender As Object, e As EventArgs) Handles TextAmp.MouseLeave
        LabelInfo.Text = "INFO :"
    End Sub

    Private Sub TextDump_MouseHover(sender As Object, e As EventArgs) Handles TextDump.MouseHover
        LabelInfo.Text = "INFO : Please enter damping. Use comma as decimal point."
    End Sub

    Private Sub TextDump_MouseLeave(sender As Object, e As EventArgs) Handles TextDump.MouseLeave
        LabelInfo.Text = "INFO :"
    End Sub

    Private Sub TextPer_MouseHover(sender As Object, e As EventArgs) Handles TextPer.MouseHover
        LabelInfo.Text = "INFO : Please enter period. Period must be greather than 0. Use comma as decimal point."
    End Sub

    Private Sub TextPer_MouseLeave(sender As Object, e As EventArgs) Handles TextPer.MouseLeave
        LabelInfo.Text = "INFO :"
    End Sub

    Private Sub TextTime_MouseHover(sender As Object, e As EventArgs) Handles TextTime.MouseHover
        LabelInfo.Text = "INFO : Please enter time to start. Time cannot be negative. Use comma as decimal point."
    End Sub

    Private Sub TextTime_MouseLeave(sender As Object, e As EventArgs) Handles TextTime.MouseLeave
        LabelInfo.Text = "INFO :"
    End Sub
End Class
