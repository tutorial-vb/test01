﻿Public Class Form2

    Public Sub Pendel_Draw() '(ByVal amp As Single, ByVal t As Single, ByVal TT As Single, ByRef x As Single, ByRef y As Single)
        Dim PendelGraphic As Graphics = CreateGraphics()
        Dim blueBrush As New Drawing.SolidBrush(Color.Blue)
        Dim redPen As New Drawing.Pen(Color.Red, 5)
        Dim blackPen As New Drawing.Pen(Color.Black, 10)
        PendelGraphic.Clear(Me.BackColor)
        ''For t = 0 To 20 Step 1
        ''Form1.x = Form1.Amp * Math.Cos(2 * Math.PI / Form1.TT * Form1.t) 'X position

        Form1.x = Form1.Amp * Math.E ^ (1 - (Math.E ^ (Form1.damp * Form1.t))) * Math.Sin(2 * Math.PI / Form1.TT * Form1.t) 'Y position
        Form1.y = Math.Sqrt(Form1.Amp ^ 2 - Form1.x ^ 2)
        Form1.y = Math.Abs(Form1.y)

        PendelGraphic.DrawLine(redPen, Convert.ToSingle(Me.Size.Width / 2), 50, Convert.ToSingle(Me.Size.Width / 2) + Form1.x, Form1.y + 50)
        PendelGraphic.DrawEllipse(blackPen, Convert.ToSingle(Me.Size.Width / 2) - 10 + Form1.x, Form1.y + 40, 20, 20)
        ''Next t
    End Sub

    Private Sub Form2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Pendel_Draw()
        Me.Location = New Point(Form1.Location.X + Form1.Size.Width, Form1.Location.Y)
    End Sub
End Class